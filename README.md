Contact
========


**Contact** is an attempt at sentiment analysis using location data from tweets to try and determine patterns of growth and decay of happiness and sadness in the world. The tweets that are being tracked are mined from the twitter streaming API and only the ones tagged with a **:)** and **:(**. By tracking location metadata from tweets, we hope to determine if the emoticon tagged tweets spread like disease contact network patterns. The initial stages of the project will be to set up the web app to track the location data in real time. **Mapbox** and **Leaflet.js** are the technologies that will be used to track the location metadata.  

This code is being developed by **Todd Ervin** at the **University of North Carolina Asheville**. The mathematical analysis is being performed by **Janine Haugh, Phd**. This application
is in development. Please be aware of this as you look at the code. 


**REMINDER:** This app is under construction and exists in a constant state of development   