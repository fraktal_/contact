#! /usr/bin/python  

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import sys
import credentials
import json
import jsonpickle
import logging
import re  
import csv

#credentials for Twitter OAuth 
CONSUMER_KEY = credentials.CONSUMER_KEY
CONSUMER_SECRET = credentials.CONSUMER_SECRET
ACCESS_TOKEN = credentials.ACCESS_TOKEN
ACCESS_TOKEN_SECRET = credentials.ACCESS_TOKEN_SECRET

# open a CSV file to write geo data
csv_file = "happy.csv"
happy_csv = csv.writer(open(csv_file, 'wb'), delimiter=',') 
happy_csv.writerow([ 'date','time', 'latitude', 'longitude', 'emoticon' ])

csv_file = "sad.csv"
sad_csv = csv.writer(open(csv_file, 'wb'), delimiter=',') 
sad_csv.writerow([ 'date','time', 'latitude', 'longitude', 'emoticon' ])

class StdOutListener(StreamListener):
  
    #Twitter Streaming API
    def on_status(self, status):     

         #jsonpickle defines complex Python model objects and turns the objects into JSON 
         tweets = json.loads(jsonpickle.encode(status))

         #tagging geo data with happy emoticon
         if re.search('(:\))', status.text):
            if tweets["geo"]:
                date = status.created_at.date().strftime("20%y/%m/%d")  
                time = status.created_at.time().strftime("%H:%M:%S")#GMT time for world wide tweet geo data  
                latitude = tweets["geo"]["coordinates"][0]
                longitude = tweets["geo"]["coordinates"][1]
                emoticon = ':)'
                
                #write data to CSV file
                happy_csv.writerow([ date, time, latitude, longitude, emoticon ]) 
            
         
         #tagging geo data with sad emoticon 
         if re.search('(:\()', status.text):
            if tweets["geo"]:
                date = status.created_at.date().strftime("20%y/%m/%d")  
                time = status.created_at.time().strftime("%H:%M:%S")#GMT time for world wide tweet geo data  
                latitude = tweets["geo"]["coordinates"][0]
                longitude = tweets["geo"]["coordinates"][1]
                emoticon = ':('
                
                #write data to CSV file
                sad_csv.writerow([ date, time, latitude, longitude, emoticon ]) 
           

    #error handling
    def on_error(self, error):
        print error 

    
     
if __name__ == '__main__':
    listener = StdOutListener()
    auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN,ACCESS_TOKEN_SECRET )
    print >> sys.stderr,"retrieving data......"
    stream = Stream(auth, listener)    
    stream.filter(track=[':)', ':('])